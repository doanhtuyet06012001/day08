<?php
session_start();
$gender = array("Nam", "Nữ");
$falcutyArr = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");

?>
<!DOCTYPE html>
<html lang="vi">

<head>
  <meta charset="UTF-8">

  <!-- base style  -->
  <link rel="stylesheet" href="styles.css">

  <!-- bootstrap -->
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
  <title>Student Infor</title>
</head>

<body>

  <body>
    <div class="container-box">
      <div class="search-box">
        <form id="search-form" action="" method="get">
          <div class="info-col">
            <label for="falcutyArr" class="h-100 industry">Khoa</label>
            <select name="falcutyArr" id="falcutyArr" class='h-100'>
              <option value="" disabled selected value>Chọn phân khoa</option>
              <?php
              foreach ($falcutyArr as $key => $value) {
                if ($_GET['falcutyArr'] == $key) {
                  echo "<option value='$key' selected>$value</option>";
                } else {
                  echo "<option value='$key'>$value</option>";
                }
              }
              ?>
            </select>
          </div>
          <div class="info-col">
            <label for="" class="h-100">Từ khóa</label>
            <?php
            if (isset($_GET['keyword'])) {
              $keyword = $_GET['keyword'];
            } else {
              $keyword = "";
            }
            echo "<input type='text' name='keyword' id='keyword' class='h-100' value='$keyword'>";
            ?>
          </div>
          <div class="btn-group">
            <div class="btn">
              <input id="submit" type="submit" value="Tìm kiếm"></input>
            </div>
            <div class="btn">
              <input id="submit" name="clear" type="button" onclick="clearForm()" value="Xóa"></input>
            </div>
          </div>
        </form>
      </div>

      <div class='row-infor'>
        <p><b> Số sinh viên tìm thấy: XXX</b></p><a class='add' href="signIn.php"> Thêm </a>
      </div>


      <table>
        <tr>
          <th>No.</th>
          <th>Tên sinh viên</th>
          <th>Khoa</th>
          <th>Action</th>
        </tr>
        <tr>
          <td>1</td>
          <td>Nguyễn Văn A</td>
          <td>Khoa học máy tính</td>
          <td>
            <button class='btn'> Xóa</button>
            <button class='btn'>Sửa</button>
          </td>
        </tr>
        <tr>
          <td>2</td>
          <td>Trần Thị B</td>
          <td>Khoa học máy tính</td>
          <td>
            <button class='btn'> Xóa</button>
            <button class='btn'>Sửa</button>
          </td>
        </tr>
        <tr>
          <td>3</td>
          <td>Nguyễn Hoàng C</td>
          <td>Khoa học vật liệu</td>
          <td>
            <button class='btn'> Xóa</button>
            <button class='btn'>Sửa</button>
          </td>
        </tr>
        <tr>
          <td>4</td>
          <td>Đình Quang D</td>
          <td>Khoa học vật liệu</td>
          <td>
            <button class='btn'> Xóa</button>
            <button class='btn'>Sửa</button>
          </td>
        </tr>
      </table>

    </div>
  </body>

  <script>
    function clearForm() {
      document.getElementById("falcutyArr").value = "";
      document.getElementById("keyword").value = "";
    }
  </script>


</html>